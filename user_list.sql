CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `vw_user_enquiry` AS
    SELECT 
        `ul`.`user_id` AS `user_id`,
        `ul`.`full_name` AS `full_name`,
        `e`.`enquiry_id` AS `enquiry_id`
    FROM
        (`tbl_user_list` `ul`
        LEFT JOIN `tbl_enquiry` `e` ON (`ul`.`user_id` = `e`.`client_id`))