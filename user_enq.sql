CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `db_lite_saleslogics_in_live`.`vw_user_enquiry_list` AS
    SELECT 
        `e`.`enquiry_id` AS `enq_id`,
        `e`.`client_id` AS `client_id`,
        `ul`.`user_id` AS `user_id`,
        `ul`.`full_name` AS `full_name`
    FROM
        (`db_lite_saleslogics_in_live`.`tbl_enquiry` `e`
        LEFT JOIN `db_lite_saleslogics_in_live`.`tbl_user_list` `ul` ON (`e`.`enquiry_id` = `ul`.`user_id`))